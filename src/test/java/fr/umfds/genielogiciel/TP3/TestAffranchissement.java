package fr.umfds.genielogiciel.TP3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import poste.Colis;
import poste.Lettre;
import poste.Recommandation;


public class TestAffranchissement {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
	
	//valeurabsolue(expected-actual) < delta
	@Test
	public void testTA1() {
		assertEquals(lettre1.tarifAffranchissement(), tolerancePrix, 1F);
	}
	
	@Test
	public void testTA2() {
		assertEquals(2.3F, lettre2.tarifAffranchissement(), tolerancePrix);
	}
	
	@Test
	public void testTA3() {
		assertEquals(3.5F, colis1.tarifAffranchissement(), tolerancePrix);
	}

}
