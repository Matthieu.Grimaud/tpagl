package fr.umfds.genielogiciel.TP3;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.Colis;
import poste.Lettre;
import poste.Recommandation;
import poste.SacPostal;

public class TestPostal {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200); 
	
	SacPostal sac1 = new SacPostal();
	SacPostal sac2 = sac1.extraireV1("7877");
	
	 /*@BeforeEach
	    public void init() {
		 sac1.ajoute(lettre1);
		 sac1.ajoute(lettre2);
		 sac1.ajoute(colis1);
	 }*/
	 
	 @Test
	 	public void TestS1() {
		 sac1.ajoute(lettre1);
		 sac1.ajoute(lettre2);
		 sac1.ajoute(colis1);
		 assertEquals(116.5,sac1.valeurRemboursement(),tolerancePrix);
	 }
	 @Test
	 	public void TestS2() {
		 sac1.ajoute(lettre1);
		 sac1.ajoute(lettre2);
		 sac1.ajoute(colis1);
		 assertEquals(0.025359999558422715f,sac1.getVolume(),toleranceVolume);
	 }
	 @Test
	 	public void TestS3() {
		 assertEquals(toleranceVolume,sac2.getVolume(),0.02517999955569394f);
	 }
}
