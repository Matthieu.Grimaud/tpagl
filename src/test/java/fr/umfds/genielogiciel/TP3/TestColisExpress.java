package fr.umfds.genielogiciel.TP3;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import poste.ColisExpress;
import poste.Recommandation;



public class TestColisExpress {
    private static float tolerancePrix=0.001f;
    

    //dsbfvdbffdb
    //gdvsd sfdbsfb
    //dfvdbdfbdf
    //sdvvbfdvb
    @Test
    public void affranchissement() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(Math.abs(colisExpress1.tarifAffranchissement()-33.0f)<tolerancePrix);
        } catch(Exception e) {
            assertTrue(false);
        }
    }

    @Test 
    public void string() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(colisExpress1.toString().equals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/25.0/0"));
        } catch(Exception e) {
            assertTrue(false);
        }
    }

    @Test
    public void remboursement() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(Math.abs(colisExpress1.tarifRemboursement()-100.0f)<tolerancePrix);
        } catch(Exception e) {
            assertTrue(false);
        }
    }

    @Test
    public void remboursement2() {
        try {
            ColisExpress colisExpress2 = new ColisExpress("vdsv", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(Math.abs(colisExpress1.tarifRemboursement()-100.0f)<tolerancePrix);
        } catch(Exception e) {
            assertTrue(false);
        }
    }
    @Test
    public void invalide() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(false);
            colisExpress1.affiche();
        } catch(Exception e) {
            assertTrue(e.getMessage().equals("poids incohérent, votre colis ne pourra pas être acheminé."));
        }
    }
    
}
/*
import poste.Colis;
import poste.ColisExpress;
import poste.ColisExpressInvalide;
import poste.Recommandation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class TestColisExpress{
	
	@Test
	public void testExceptionColisExpress(){
		boolean exception= false;
		
		try {
			ColisExpress ce1= new ColisExpress("Salut","Lyon","69000",30f,3f,Recommandation.deux,"fe",5f,true);
		} catch (ColisExpressInvalide e) {
			exception = true;
		}	
		assertTrue(exception);
	}
	
	@Test
	public void testExceptionColisExpress2(){
		boolean exception= false;
		
		try {
			ColisExpress ce2= new ColisExpress("Salut","Lyon","69000",9f,3f,Recommandation.deux,"fe",5f,true);
		} catch (ColisExpressInvalide e) {
			exception = true;
		}	
		assertFalse(exception);
	}
	
	/*@Test
	public void testExceptionColisExpress3() throws ColisExpressInvalide {
		boolean exception= false;
		
		try {
			ColisExpress ce2= new ColisExpress("Salut","Lyon","69000",29.99f,3f,Recommandation.deux,"fe",5f,true);
		} catch (ColisExpressInvalide e) {
			exception = true;
		}	
		assertFalse(exception);
	}
}*/
